from argon2 import PasswordHasher
from argon2.exceptions import (
    InvalidHash,
    VerifyMismatchError
)
from cryptography.fernet import Fernet
from typing import NoReturn, Self


class SecretBox:
    """
        Responsible to generate and verify secrets.

    """
    
    __argon = PasswordHasher()

    def __init__(
        self: Self,
        key: str
    ) -> NoReturn:
        self.__fernet = Fernet(key)

    @classmethod
    @property
    def randomic(
        self: Self
    ) -> str:
        """
            Generates randomic keys.
        """
        return Fernet.generate_key().decode()

    def generate(
        self: Self,
        secret: str
    ) -> str:
        """
            Generates password keys from a hash.
        """
        return self.__argon.hash(
            secret
        )

    def verify(
        self: Self,
        hash: str,
        secret: str,
    ) -> bool:
        """
            Verify password hash.
        """
        try:
            result = self.__argon.verify(
                hash,
                secret
            )
        except InvalidHash:
            return False
        except VerifyMismatchError:
            return False
        return result

    def encrypt(
        self: Self,
        message: str
    ) -> str:
        """
            Encrypt messages.
        """
        return self.__fernet.encrypt(
            str(
                message
            ).encode()
        ).decode()

    def decrypt(
        self: Self,
        message: str
    ) -> str:
        """
            Decrypt messages.
        """
        return self.__fernet.decrypt(
            message if isinstance(
                message,
                bytes
            ) else message.encode()
        ).decode()
