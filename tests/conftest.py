from cryptography.fernet import Fernet
from pytest import fixture
from random import choice
from secretbox import SecretBox
from string import printable


@fixture
def secret():
	return SecretBox(SecretBox.randomic)

@fixture
def message():
	text = ''
	for string in range(10):
		text += choice(printable)
	return text