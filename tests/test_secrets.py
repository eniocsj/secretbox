from argon2.exceptions import VerificationError
from pytest import raises

def test_randomic(secret):
	assert isinstance(secret.randomic, str)

def test_encrypt_message(secret, message):
	encrypted_message = secret.encrypt(message)
	assert isinstance(encrypted_message, str)
	assert secret.decrypt(encrypted_message) == message

def test_hash(secret, message):
	hash_message = secret.generate(message)
	assert secret.verify(hash_message, message)

def test_wrong_message(secret, message):
	hash_message = secret.generate(message)
	assert secret.verify(hash_message, 'wrong_hash') == False

def test_wrong_hash(secret, message):
	hash_message = secret.generate(message)
	assert secret.verify('wrong_hash', message) == False