# <center>Secret Box</center>

This package makes it easy to create encryption and decryption of messages, as well as creating random keys

## How to use

```python
>>> from secretbox import SecretBox
>>> message = 'V3rY$7r0n6p45$w0rD4N07h!n6'
>>> key = SecretBox.randomic
>>> key
'WKBq1FveR-VwkPu-CpUtCuCKPW7TKu0RJkikNyyaUh8='
>>> secret = SecretBox(key=key)
>>> hash_message = secret.generate(secret=message)
>>> hash_message
'$argon2id$v=19$m=65536,t=3,p=4$BrefVjC7k7l3t53t2xKgWA$ZLyuq6jkwmTUoo41F/p3iwhuDzQ3AtuAMt1VT7XuA1g'
>>> secret.verify(hash_message, message)
True
>>> encrypted_message = secret.encrypt(message)
>>> encrypted_message
'gAAAAABmYfFocQECVXaaNv3BJhfJcBDYbsmQUy_uY5V_DIMl_gn8fkivZwWfrEQHZ_sSVnOgQm2DTEVqT3QFcnWw3iqKlBZ_-Nsa0TKO2yYdgykUNDakoT4='
>>> secret.decrypt(encrypted_message)
'V3rY$7r0n6p45$w0rD4N07h1n6'
```